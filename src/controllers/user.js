const passport = require('passport');
const validator = require('validator');
const {User} = require('../models/user');

/**
 * GET /login
 * Login page.
 */
exports.getLogin = (req, res) => {
  res.render('account/login', {
    title: 'Login'
  });  
};

/**
 * POST /login
 * Sign in using email and password.
 */
exports.postLogin = (req, res, next) => {
  const validationErrors = [];
  if (!validator.isEmail(req.body.email)) validationErrors.push({ msg: 'Please enter a valid email address.' });
  if (validator.isEmpty(req.body.password)) validationErrors.push({ msg: 'Password cannot be blank.' });

  if (validationErrors.length) {
    console.log(validationErrors);
    return res.redirect('/login');
  }
  req.body.email = validator.normalizeEmail(req.body.email, { gmail_remove_dots: false });

  passport.authenticate('local', (err, user, info) => {
    if (err) { return next(err); }
    if (!user) {
      console.log(info);
      return res.redirect('/login');
    }
    req.logIn(user, (err) => {
      if (err) { return next(err); }
      console.log('Success! You are logged in.');
      req.session.user = user; 
      return res.redirect('/account');
    });
  })(req, res, next);
};

/**
 * GET /logout
 * Log out.
 */
exports.logout = (req, res) => {
  if (req.session.user && req.cookies.user_sid) {
    res.clearCookie('user_sid');
    req.logout();
    req.session.destroy((err) => {
      if (err) console.log('Error : Failed to destroy the session during logout.', err);
      req.user = null;
      res.redirect('/');
    });  
  } 
  else {
    res.redirect('/login');
  }
};

/**
 * GET /signup
 * Signup page.
 */
exports.getSignup = (req, res) => {
  if (req.user) {
    return res.redirect('/');
  }
  res.render('account/signup', {
    title: 'Create Account'
  });
};

/**
 * POST /signup
 * Create a new local account.
 */
exports.postSignup = async (req, res, next) => {  
  const validationErrors = [];
  if (!validator.isEmail(req.body.email)) validationErrors.push({ msg: 'Please enter a valid email address.' });
  if (!validator.isLength(req.body.password, { min: 6 })) validationErrors.push({ msg: 'Password must be at least 8 characters long' });
  if (req.body.password !== req.body.confirmPassword) validationErrors.push({ msg: 'Passwords do not match' });

  if (validationErrors.length) {
    console.log(validationErrors);
    return res.redirect('/signup');
  }
  req.body.email = validator.normalizeEmail(req.body.email, { gmail_remove_dots: false });

  const userData = req.body; 
  const isUserExists = await User.findOne({
    where:{
      email: userData.email
    }
  });
  if( isUserExists ){
    console.log('Account with that email address already exists.'); 
    return res.redirect('/signup');
  }
  else{
    await User.create(userData);
    req.session.user = userData;
    console.log(req.session.user);    
    console.log('User Registered');    
    return res.redirect('/');
  }
};

/**
 * GET /account
 * Profile page.
 */
exports.getAccount = (req, res) => {
  if (req.session.user && req.cookies.user_sid) {
    res.render('account/profile', {
      title: 'Account Management'
    });  
  } else {
    return res.redirect('/');
  }
};

