const express = require('express');
const compression = require('compression');
const session = require('express-session');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const dotenv = require('dotenv');
const path = require('path');
const passport = require('passport');
dotenv.config();

/**
 * Controllers (route handlers).
 */
const homeController = require('./controllers/home');
const userController = require('./controllers/user');

/**
 * API keys and Passport configuration.
 */
const passportConfig = require('./config/passport');

/**
 * Create Express server.
 */
const app = express();

/**
 * Connect to MySQL.
 */
const db = require('./config/database');
db.authenticate()
    .then(() => console.log('Database connected'))
    .catch((err) => console.log('Error while connecting database : ' + err));

/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 8080);
app.use(compression());
app.use(bodyParser.json());
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(passport.session());
app.use(session({
  key:'user_sid',
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: false,
  cookie: { 
    expires: 6000000
  }
}))

/**
 * Check if cookie exists without session 
 * clear cookie
 */
app.use((req, res, next) => {
  if (req.cookies.user_sid  && !req.session.user) {
      res.clearCookie('user_sid');        
  }
  next();
});

/**
 * Middleware for checking whether session exists or not
 */
const sessionChecker = (req, res, next) => {
  if (req.session.user && req.cookies.user_sid) {
      return res.redirect('/account');
  } else {
      next();
  }    
};

app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
// app.use(express.static(__dirname + '/public'));

/**
 * Primary app routes.
 */
app.get('/',sessionChecker,homeController.index);

app.get('/login',sessionChecker,userController.getLogin);
app.post('/login', userController.postLogin);

app.get('/logout', userController.logout);

app.get('/signup',sessionChecker,userController.getSignup);
app.post('/signup', userController.postSignup);

app.get('/account',userController.getAccount);


/**
 * OAuth authentication routes. (Sign in)
 */
app.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'], accessType: 'offline', prompt: 'consent' }));
app.get('/auth/google/callback', passport.authenticate('google', { failureRedirect: '/login' }), (req, res) => {
  return res.redirect('/account');
});


/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
  console.log('App is running at http://localhost:%d ', app.get('port'));
  console.log('Press CTRL-C to stop\n');
});

module.exports = app;