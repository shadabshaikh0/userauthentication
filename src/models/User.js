const {DataTypes} = require('sequelize');
const db = require('../config/database');
const User = db.define('User', {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    mobileno: DataTypes.STRING,
    password: DataTypes.STRING
});

module.exports = {
  User
};
