const passport = require('passport');
const refresh = require('passport-oauth2-refresh');
const { Strategy: LocalStrategy } = require('passport-local');
const { OAuth2Strategy: GoogleStrategy } = require('passport-google-oauth');
const {User} = require('../models/user');

passport.serializeUser((user, done) => {
  done(null, user.email);
});

passport.deserializeUser(async(email, done) => {
  const isUserExists = await User.findOne({
    where:{
      email:email
    }
  });
  (isUserExists) ? done(null,isUserExists) : done('User not found',null);
});

/**
 * Sign in using Email and Password.
 */
passport.use(new LocalStrategy({ usernameField: 'email' }, async (email, password, done) => {
  const user = await User.findOne({
    where:{
      email:email
    }
  });
  if(user) {  
    if(user.password === password){
      return done(null, user);
    }
    else{
      return done(null, false, { msg: 'Invalid email or password.' });
    }
  }
  else{
    return done(null, false, { msg: 'Invalid email or password.' });
  }
}));


// /**
//  * Sign in with Google.
//  */
const googleStrategyConfig = new GoogleStrategy({
    clientID: process.env.GOOGLE_ID,
    clientSecret: process.env.GOOGLE_SECRET,
    callbackURL: '/auth/google/callback',
    passReqToCallback: true
  }, async (req, accessToken, refreshToken, params, profile, done) => {
    const email = profile.emails[0].value;
    const name  = profile.displayName;
    const userData = {name,email};
    const existingUser = await User.findOne({
      where:{
        email: email
      }
    });
    req.session.user = userData;
    if(existingUser){
      console.log('User Exists');      
      done(null, existingUser);
    }
    else{
      await User.create(userData);
      console.log('New User Created');
      done(null,userData);
    }
  });
  passport.use('google', googleStrategyConfig);
  refresh.use('google', googleStrategyConfig);
    