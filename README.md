# UserAuthentication

This project is a solution for the following problem statement:<br>
<h1>Objective: Google Auth for Page</h1><br>
<ol>
    <li>Create Three Pages, Login, LoggedIn Page, Register</li>
    <li>In DB it takes Email, Password, Mobile, Name</li>
    <li>Create APIs in express</li>
    <li>DB can be your local</li>
    <li>Implement both Normal Login and Google Login</li>
    <li>If Google Login Email Matches existing email, authorize the user at that point</li>
</ol>

<h1>Demo</h1>
<a href="http://fda5dc55.ngrok.io">
    Open this link for demo
</a>

 

<h1>Setup</h1>
Install all dependencies mentioned in package.json<br>
Run following command to install 
<code>
npm install 
</code>

<h1> How to run</h1>
Use following command to run server
<code>node server.js</code>
and
<a href="http://localhost:8080">
    Open this link
</a>


<h1>TODO:</h1>
<ol>
    <li>Validation on frontend</li>
    <li>User Interface</li>
    <li>Add Linter</li>
    <li>Add Tests</li>
    <li>Dockerise Application</li>
</ol>
